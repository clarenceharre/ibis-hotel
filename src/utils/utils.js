export const REG = {
  // TYPES: ["DATA", "TIME"],
  DATA: [
    {
      LIMIT: "104857600",
      PRICE: "$0.50",
    },
    {
      LIMIT: "524288000",
      PRICE: "$1",
    },
    {
      LIMIT: "1073741824",
      PRICE: "$2",
    },
    {
      LIMIT: "2147483648",
      PRICE: "$3",
    },
    {
      LIMIT: "4294967296",
      PRICE: "$5",
    },
  ],
  // TIME: [
  //   {
  //     LIMIT: "3 hours",
  //     PRICE: "$5",
  //   },
  //   {
  //     LIMIT: "12 hours",
  //     PRICE: "$10",
  //   },
  //   {
  //     LIMIT: "48 hours",
  //     PRICE: "$20",
  //   },
  // ],
};

export const formatUsage = (bytes) => {
  var sizes = ["Bytes", "KB", "MB", "GB", "TB"];
  if (bytes === undefined || Number.isNaN(bytes) || bytes == 0)
    return "0 Bytes";
  var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
  return Math.round((bytes * 10) / Math.pow(1024, i), 2) / 10 + " " + sizes[i];
};
