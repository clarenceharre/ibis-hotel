import React, { useEffect, useState } from "react";
import { Row } from "reactstrap";
import "devextreme/dist/css/dx.softblue.css";
import LoginPage from "./LoginPage";
import Cookies from "universal-cookie";
import config from "config";
import GuestDashboard from "./GuestDashboard";
import AdminDashboard from "./AdminDashboard";

function Dashboard() {
  const cookies = new Cookies();
  const [validUser, setValidUser] = useState(true);
  const [admin, setAdmin] = useState(false);
  const [userInformation, setUserInformation] = useState(null);

  const exportValidUser = () => {
    setValidUser(true);
  };

  useEffect(() => {
    let user = cookies.get("user");
    switch (user) {
      case undefined:
        setValidUser(false);
        break;
      case "Admin":
        fetch(`${config.backendUrlPrefix}/fetch-all-users`)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            setUserInformation(data);
            console.log(data);
          });
        setAdmin(true);
        break;
      default:
        fetch(`${config.backendUrlPrefix}/fetch-user?username=${user}`)
          .then((response) => {
            return response.json();
          })
          .then((data) => {
            setUserInformation(data);
            console.log(data);
          });
    }
  }, [validUser]);

  return (
    <div className="content">
      <LoginPage exportValidUser={exportValidUser} validUser={validUser} />
      <Row>
        {userInformation !== null && admin === false && (
          <GuestDashboard userInformation={userInformation} />
        )}
        {userInformation !== null && admin == true && (
          <AdminDashboard userList={userInformation} />
        )}
      </Row>
    </div>
  );
}

export default Dashboard;

/*
<Card className="card-stats">
  <CardBody>
    <Row>
      <Col md="4" xs="5">
        <div className="icon-big text-center icon-warning">
          <i className="nc-icon nc-globe text-warning" />
        </div>
      </Col>
      <Col md="8" xs="7">
        <div className="numbers">
          <p className="card-category">Capacity</p>
          <CardTitle tag="p">150GB</CardTitle>
          <p />
        </div>
      </Col>
    </Row>
  </CardBody>
  <CardFooter>
    <hr />
    <div className="stats">
      <i className="fas fa-sync-alt" /> Update Now
    </div>
  </CardFooter>
</Card>
*/

// https://js.devexpress.com/Demos/WidgetsGallery/Demo/Form/Validation/React/Light/
