import React from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import "devextreme/dist/css/dx.softblue.css";
import { formatUsage } from "utils/utils";
import PieChart, { Series } from "devextreme-react/pie-chart";
import $ from "jquery";
import DataGrid, { Column } from "devextreme-react/data-grid";

function GuestDashboard({ userInformation }) {
  return (
    <div className="content">
      <Row>
        <Col sm={6}>
          <Card className="card-stats">
            <CardHeader>
              <CardTitle tag="h4">Profile Details</CardTitle>
            </CardHeader>
            <CardBody>
              Welcome <b>{userInformation.username}</b>, <br />
              <br /> You currently have <b>
                {userInformation.deviceCount}
              </b>{" "}
              connected devices. You have used{" "}
              <b>{formatUsage(userInformation.usage)}</b> of your{" "}
              <b>{formatUsage(userInformation.limit)}</b> limit. <br />
              <br />
              <div id="piechart" style={{ width: "45vw" }}>
                <PieChart
                  palette="Bright"
                  dataSource={[
                    {
                      label: "Remaining Capacity",
                      value: userInformation.limit - userInformation.usage,
                    },
                    { label: "Amount Used", value: userInformation.usage },
                  ]}
                  size={$("#piechart").width()}
                >
                  <Series argumentField="label" valueField="value" />
                </PieChart>
              </div>
            </CardBody>
            <CardFooter />
          </Card>
        </Col>
        <Col sm={6}>
          <Card className="card-stats">
            <CardHeader>
              <CardTitle tag="h4">Connected Devices</CardTitle>
            </CardHeader>
            <CardBody>
              <DataGrid
                dataSource={userInformation.deviceList}
                showBorders
                rowAlternationEnabled
              >
                <Column dataField="name" />
                <Column
                  dataField="usage"
                  cellRender={(d) => {
                    return <div>{formatUsage(d.value)}</div>;
                  }}
                />
              </DataGrid>
            </CardBody>
            <CardFooter />
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default GuestDashboard;
