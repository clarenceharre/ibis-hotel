import "devextreme/dist/css/dx.softblue.css";
import React, { useEffect, useState } from "react";
import { Card, CardBody, CardHeader, CardTitle } from "reactstrap";
import Form, {
  EmptyItem,
  PatternRule,
  RequiredRule,
} from "devextreme-react/form";
import { GroupItem, SimpleItem, ButtonItem } from "devextreme-react/form";
import { DataGrid, LoadIndicator, Popup, ScrollView } from "devextreme-react";
import { formatUsage, REG } from "../utils/utils";
import config from "config";
import Cookies from "universal-cookie";
import { Column } from "devextreme-react/data-grid";

function LoginPage({ exportValidUser, validUser }) {
  const cookies = new Cookies();
  const [simHelper, setSimHelper] = useState(5);
  const [register, setRegister] = useState(false);
  const [paymentMade, setPaymentMade] = useState(false);
  const [registerInformation, setRegisterInformation] = useState({
    /* USER DETAILS */
    username: null,
    password: null,

    /* PAYMENT INFORMATION */
    cardNumber: null,
    cardExpiry: null,
    cardCSV: null,

    /* USAGE INFORMATION */
    // type: REG.TYPES[0],
    limit: REG.DATA[0],
  });
  const user = {
    username: null,
    password: null,
  };

  /* HANDLE APIs */

  const handleLoginSubmit = () => {
    let requestOpts = {
      method: "POST",
      body: JSON.stringify(user),
      headers: {
        "Content-Type": "application/json",
      },
    };
    console.log(requestOpts);
    fetch(`${config.backendUrlPrefix}/login`, requestOpts).then((response) => {
      console.log(response);
      switch (response.status) {
        case "200" | 200:
          console.log("Valid User", user.username);
          cookies.set("user", user.username, { path: "/" });
          exportValidUser();
          break;
        case "404" | 404:
          console.error("User does not exist.");
          break;
        case "401" | 401:
          console.error("User could not be authenticated.");
          break;
      }
    });
  };

  const handleRegisterSubmit = () => {
    let requestOpts = {
      method: "POST",
      body: JSON.stringify(registerInformation),
      headers: {
        "Content-Type": "application/json",
      },
    };
    console.log(requestOpts);
    fetch(`${config.backendUrlPrefix}/add-user`, requestOpts).then(
      (response) => {
        console.log(response);
        switch (response.status) {
          case "200" | 200:
            cookies.set("user", registerInformation.username, { path: "/" });
            setPaymentMade(true);
            simulation();
            break;
          case "204" | 204:
            console.error("User does not exist.");
            break;
          case "401" | 401:
            console.error("User could not be authenticated.");
            break;
        }
      }
    );
  };

  /* SIMULATION */

  const sleep = (ms) => {
    return new Promise((resolve) => setTimeout(resolve, ms));
  };

  async function simulation() {
    for (let i = 1; i < 5; i++) {
      await sleep(1 * 1000);
      setSimHelper(5 - i);
    }
    exportValidUser();
  }

  function handleFieldChange(e) {
    console.log(e);
    setRegisterInformation({ ...registerInformation, [e.dataField]: e.value });
  }

  return (
    <Popup visible={!validUser} showTitle={false}>
      <ScrollView>
        <Card>
          {paymentMade && (
            <>
              <CardHeader>
                <CardTitle tag="h4">Payment Successful</CardTitle>
              </CardHeader>
              <CardBody>
                Thank you for registering your device.
                <b> You may freely browse the internet now.</b>
                <br />
                <br />
                Please wait. You will be redirected to the dashboard in a
                moment.
                <br />
                <br />
                <div style={{ textAlign: "right" }}>
                  {simHelper} <LoadIndicator width="20px" height="20px" />
                </div>
              </CardBody>
            </>
          )}

          {!paymentMade && (
            <>
              <div style={{ textAlign: "center" }}>
                <CardHeader>
                  <CardTitle tag="h4">Welcome to Ibis-Hotel</CardTitle>
                </CardHeader>
                <CardBody>
                  Your device is not currently registered for internet access.
                  <b> Please login or register a new account below.</b>
                </CardBody>
              </div>

              <CardBody>
                <Form
                  visible={!register}
                  formData={user}
                  labelMode="static"
                  width="30vw"
                  style={{
                    display: "block",
                    margin: "auto",
                    width: "40%",
                  }}
                >
                  <SimpleItem dataField="username" isRequired />
                  <SimpleItem
                    dataField="password"
                    isRequired
                    editorType="dxTextBox"
                    editorOptions={{ mode: "password" }}
                  />
                  <GroupItem colCount={2}>
                    <ButtonItem
                      buttonOptions={{
                        text: "REGISTER",
                        useSubmitBehavior: true,
                        stylingMode: "contained",
                        type: "default",
                        width: "100%",
                        onClick: () => {
                          setRegister(true);
                        },
                      }}
                    />
                    <ButtonItem
                      buttonOptions={{
                        text: "LOGIN",
                        useSubmitBehavior: true,
                        stylingMode: "contained",
                        type: "success",
                        width: "100%",
                        onClick: handleLoginSubmit,
                      }}
                    />
                  </GroupItem>
                </Form>
                <br />
                <Form
                  formData={registerInformation}
                  colCount={2}
                  onFieldDataChanged={handleFieldChange}
                  showValidationSummary
                  visible={register}
                >
                  <GroupItem
                    colSpan={1}
                    caption="Card Details"
                    // visible={false}
                  >
                    <SimpleItem isRequired dataField="cardNumber">
                      <RequiredRule message="Card Number is Required" />
                      {/* <PatternRule
                        pattern={/^\d{16}/}
                        message="Card Number: 16 digits are required, no spaces."
                      /> */}
                    </SimpleItem>
                    <SimpleItem isRequired={true} dataField="cardExpiry">
                      <RequiredRule message="Card Expiry is Required" />
                      {/* <PatternRule
                        pattern={/^\d\d\/\d\d/}
                        message="Card Expiry: enter as mm/yy (for example, 04/22)"
                      /> */}
                    </SimpleItem>
                    <SimpleItem
                      isRequired={true}
                      dataField="cardCSV"
                      helpText="The three digit number on the back of the card."
                    >
                      <RequiredRule message="Card CSV is Required" />
                      {/* <PatternRule
                        pattern={/^\d{3,4}/}
                        message="Card CSV: use 3 (Visa, Mastercard) or 4 (Amex) digits only."
                      /> */}
                    </SimpleItem>
                  </GroupItem>
                  <GroupItem
                    colSpan={1}
                    colCount={1}
                    caption="Usage Information"
                  >
                    {/* <SimpleItem
                      dataField="type"
                      editorType="dxRadioGroup"
                      editorOptions={{
                        layout: "horizontal",
                        items: REG.TYPES,
                        onValueChanged: (e) => {
                          setRegisterInformation({
                            ...registerInformation,
                            limit: REG[e.value][0],
                          });
                        },
                      }}
                    /> */}
                    <SimpleItem>
                      <DataGrid
                        dataSource={REG.DATA}
                        showBorders
                        rowAlternationEnabled
                        focusedRowEnabled
                        defaultFocusedRowIndex={0}
                        keyExpr="LIMIT"
                        onRowClick={(e) => {
                          console.log(e.data);
                          setRegisterInformation({
                            ...registerInformation,
                            limit: e.data.LIMIT,
                          });
                        }}
                      >
                        <Column
                          dataField="LIMIT"
                          cellRender={(d) => {
                            return <div>{formatUsage(d.value)}</div>;
                          }}
                        />
                        <Column dataField="PRICE" />
                      </DataGrid>
                    </SimpleItem>
                  </GroupItem>
                  <GroupItem caption="Profile Information">
                    <SimpleItem dataField="username" isRequired />
                    <SimpleItem dataField="password" isRequired />
                  </GroupItem>
                  <GroupItem>
                    <EmptyItem />
                    <EmptyItem />
                    <ButtonItem
                      buttonOptions={{
                        text: "MAKE PAYMENT",
                        useSubmitBehavior: true,
                        stylingMode: "contained",
                        type: "success",
                        width: "100%",
                        onClick: handleRegisterSubmit,
                      }}
                    />
                    <ButtonItem
                      buttonOptions={{
                        text: "RETURN TO LOGIN",
                        useSubmitBehavior: true,
                        stylingMode: "contained",
                        type: "default",
                        width: "100%",
                        onClick: () => {
                          setRegister(false);
                        },
                      }}
                    />
                  </GroupItem>
                </Form>
              </CardBody>
            </>
          )}
        </Card>
      </ScrollView>
    </Popup>
  );
}

export default LoginPage;

{
  /* <SimpleItem colSpan={1}>
                  <Button
                    color={paymentProcessing ? "info" : "success"}
                    block={true}
                    onClick={handlePayment}
                  >
                    {paymentProcessing ? (
                      <LoadIndicator
                        visible={paymentProcessing}
                        height={20}
                        width={20}
                      />
                    ) : (
                      "Make Secure Payment"
                    )}
                  </Button>
                </SimpleItem> */
}

// import React, { useEffect, useState, useRef } from "react";
// import Form, { SimpleItem, ButtonItem } from "devextreme-react/form";
// import { Button } from "reactstrap";
// import config from "config";
// import { Popup } from "devextreme-react";
// import Cookies from "universal-cookie";

// function Login() {
//   const [cookieExists, setCookieExists] = useState(true);
// const [user, setUser] = useState({
//   name: null,
//   password: null,
// });

//   const cookies = new Cookies();

//   useEffect(() => {
//     cookies.get("user") === undefined && setCookieExists(false);
//   }, []);

//   const handleFieldDataChange = (e) => {
//     setUser({ ...user, [e.dataField]: e.value });
//   };

// const handleSubmit = () => {
//   fetch(
//     `${config.backendUrlPrefix}/login?user=${user.name}&password=${user.password}`,
//     { method: "POST" }
//   ).then((response) => {
//     switch (response.status) {
//       case "202" | 202:
//         cookies.set("user", user, { path: "/" });
//         setCookieExists(true);
//         break;
//       case "204" | 204:
//         console.error("User does not exist.");
//         break;
//       case "401" | 401:
//         console.error("User could not be authenticated.");
//         break;
//     }
//   });
// };

//   return (
//     <div id="login">
//       <Form
//         formData={user}
//         onFieldDataChanged={handleFieldDataChange}
//         labelMode="static"
//       >
//         <SimpleItem dataField="name" isRequired />
//         <SimpleItem
//           dataField="password"
//           isRequired
//           editorType="dxTextBox"
//           editorOptions={{ mode: "password" }}
//         />
//         <ButtonItem
//           buttonOptions={{
//             text: "LOGIN",
//             useSubmitBehavior: true,
//             stylingMode: "contained",
//             type: "success",
//             onClick: handleSubmit,
//           }}
//         />
//       </Form>
//     </div>
//   );
// }

// export default Login;
