import React, { useState, useEffect } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardFooter,
  CardTitle,
  Col,
  Row,
} from "reactstrap";
import "devextreme/dist/css/dx.softblue.css";
import config from "config";
import { formatUsage } from "utils/utils";
import DataGrid, { Column } from "devextreme-react/data-grid";

function AdminDashboard({ userList }) {
  const [selectedUser, setSelectedUser] = useState(userList[0].username);
  const [userDevices, setUserDevices] = useState(null);
  const customRender = (d) => {
    return <div>{formatUsage(d.value)}</div>;
  };

  const handleRowClick = (e) => {
    setSelectedUser(e.key);
  };

  useEffect(() => {
    fetch(`${config.backendUrlPrefix}/fetch-user?username=${selectedUser}`)
      .then((response) => {
        return response.json();
      })
      .then((data) => {
        setUserDevices(data);
        console.log(data);
      });
  }, [selectedUser]);

  return (
    <div className="content">
      <Row>
        <Col sm={6}>
          <Card className="card-stats">
            <CardHeader>
              <CardTitle tag="h4">Connected Users</CardTitle>
            </CardHeader>
            <CardBody>
              <DataGrid
                dataSource={userList}
                showBorders
                rowAlternationEnabled
                focusedRowEnabled
                defaultFocusedRowIndex={0}
                keyExpr="username"
                onRowClick={handleRowClick}
              >
                <Column dataField="username" />
                <Column dataField="limit" cellRender={customRender} />
                <Column dataField="usage" cellRender={customRender} />
                <Column dataField="deviceCount" />
              </DataGrid>
            </CardBody>
            <CardFooter />
          </Card>
        </Col>
        <Col sm={6}>
          <Card className="card-stats">
            <CardHeader>
              <CardTitle tag="h4">Connected Devices</CardTitle>
            </CardHeader>
            <CardBody>
              {userDevices != null && (
                <DataGrid
                  dataSource={userDevices.deviceList}
                  showBorders
                  rowAlternationEnabled
                >
                  <Column dataField="name" />
                  <Column
                    dataField="usage"
                    cellRender={(d) => {
                      return <div>{formatUsage(d.value)}</div>;
                    }}
                  />
                </DataGrid>
              )}
            </CardBody>
            <CardFooter />
          </Card>
        </Col>
      </Row>
    </div>
  );
}

export default AdminDashboard;
