let serverUrl = window.SERVER_URL;
const config = {
  backendUrlPrefix: serverUrl,
  user: null,
};
export default config;
